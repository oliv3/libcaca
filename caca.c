#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <caca.h>

static void
read_file()
{
  FILE *f = fopen("test.txt", "r");
  char line[1024];
  if (NULL != fgets(line, 1024, f)) {
    line[strlen(line)-1] = 0;
    printf("line: '%s'\n", line);
    double d = strtod(line, NULL);
    printf("double: '%lf'\n", d);
  }
  fclose(f);
}

static int caca = 0;
static cucul_canvas_t *cv = NULL;
static caca_display_t *dp = NULL;
static cucul_dither_t *dither = NULL;

#define WIDTH  20
#define HEIGHT 10

static void
init_caca(void)
{
  cv = cucul_create_canvas(WIDTH, HEIGHT);
  if (NULL == cv) {
    fprintf(stderr, "cucul_create_canvas\n");
    exit(1);
  }

  dp = caca_create_display(cv);
  if (NULL == dp) {
    fprintf(stderr, "caca_create_display\n");
    exit(1);
  } else {
    caca_set_display_title(dp, "caca");
    caca_set_mouse(dp, 0);
  }

  dither = cucul_create_dither(8, WIDTH, HEIGHT, 10, 0, 0, 0, 0);
  if (NULL == dither) {
    fprintf(stderr, "cucul_create_dither\n");
  }
}

int
main(int argc, char *argv[]) {
  if (argc > 1) caca = 1;
  if (caca)  init_caca();
  read_file();
  if (caca) {
    cucul_free_dither(dither);
    caca_free_display(dp);
    cucul_free_canvas(cv);
  }
  return 0;
}
